import org.apache.spark.mllib.classification.{LogisticRegressionWithLBFGS, LogisticRegressionModel}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.{Vector, Vectors}

def mapping(input: String): Double = input match {
 case "H" => 3.0
 case "A" => 2.0
 case "HL" | "UH" => 1.0
 case "L" => 0.0
}

val data = sc.textFile("Qualitative_healthy_food.data.txt")
data.count()

val inp_modified = data.map{line => 
    val parts = line.split(",")
    LabeledPoint(mapping(parts(6)), Vectors.dense(parts.slice(0,6).map(x => mapping(x))))
}
inp_modified.take(10)

val break_data = inp_modified.randomSplit(Array(0.7, 0.3), seed = 11L)
val train_data = break_data(0)
val test_data = break_data(1)

val model = new LogisticRegressionWithLBFGS().setNumClasses(2).run(train_data)

val label_predictions = test_data.map { point =>
  val prediction = model.predict(point.features)
  (point.label, prediction)
}
val err = label_predictions.filter(r => r._1 != r._2).count.toDouble / test_data.count

println(s"Training Error => ${err}")

System.exit(0)